INSTALLATION GUIDE

sudo apt-get install python-virtualenv

python3 -m venv venv

In taburnorock directory:
source venv/bin/activate

pip install -r requirements.txt

gunicorn  --bind 0.0.0.0:5000 wsgi
gunicorn --workers 2 --timeout 120  --bind 0.0.0.0:5000 wsgi &