import sqlite3
from sqlite3 import Error
from settings import app
from flask import g

DATABASE = 'taburno.db'

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
    return db

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()

def init_db():
    with app.app_context():
        db = get_db()
        with app.open_resource('schema.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()

def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)

    return conn

def query_db(query, args=(), one=False):
    with app.app_context():

        cur = get_db().execute(query, args)
        rv = cur.fetchall()
        cur.close()
    return (rv[0] if rv else None) if one else rv


def query_data():
    users = []
    for user in query_db('select * from users'):
        users.append(user)
    return users

def query_data_by_user(username):
    user = query_db('select * from users where username = ?',
                [username], one=True)
    return user


def insert_data(users):
    """
    Insert into the users table
    :return:  id
    """
    # create a database connection
    conn = create_connection(DATABASE)
    with conn:
        sql = ''' INSERT INTO users(username, password, master_pwd)
                  VALUES(?,?,?) '''
        cur = conn.cursor()
        cur.execute(sql, users)
    return cur.lastrowid

def dbmanager(*argv):
    users = []
    if len(argv) > 2:
        insert_data(argv)
    elif len(argv) == 1 and argv[0] == "init":
        init_db()
    elif len(argv) == 1:
        user = query_data_by_user(argv[0])
        users.append(user)
    else:
         query_data()
    return users

dbmanager()
