from flask_login import UserMixin


# user model
class User(UserMixin):

    def __init__(self, id):
        self.id = id
        self.name = "user-"+id

    def __repr__(self):
        return "%d/%s/%s" % (self.id, self.name)
