from chatterbot import ChatBot
from chatterbot.trainers import ChatterBotCorpusTrainer
from chatterbot.trainers import ListTrainer
from taburno_adapter import TaburnoAdapter
# bot integration
chatbot = ChatBot('R.Giskard',
    storage_adapter='chatterbot.storage.SQLStorageAdapter',
    logic_adapters=[
        {
            'import_path': 'chatterbot.logic.BestMatch'
        },
        {
            'import_path': 'taburno_adapter.TaburnoAdapter',
            'input_text': 'file list'
        }
    ])
trainer = ChatterBotCorpusTrainer(chatbot)
# Train the chatbot based on the english corpus
trainer.train("chatterbot.corpus.english")
trainer.train("chatterbot.corpus.italian.conversations")


