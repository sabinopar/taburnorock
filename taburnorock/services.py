from settings import app, UPLOAD_FOLDER, os
import subprocess

def view_file_service(path):
    if path != '':
        total_path = UPLOAD_FOLDER + '/' + path
        total_path = os.path.relpath(total_path, UPLOAD_FOLDER)
        cmd = subprocess.Popen(['ls', '-1', total_path], stdout=subprocess.PIPE,
                stderr=subprocess.PIPE)
    else:
        relative_path = path
        cmd = subprocess.Popen(['ls', '-1', UPLOAD_FOLDER+path], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return relative_path, cmd.communicate()

