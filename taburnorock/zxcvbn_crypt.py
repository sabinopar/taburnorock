from zxcvbn import zxcvbn
import hashlib
from filecrypt import encrypt_file, decrypt_file


def zxcvbn_crypt(enc_mode, filename_path, filename_path_out, pswd):
    key = hashlib.sha256(pswd).digest()
    results = zxcvbn(str(pswd), None)
    if results['score'] < 2:
        print("PASSWORD WITH LOW SCORE: " + str(results['score']))
    elif enc_mode == "encrypt":
        print(results['score'])
        encrypt_file(key, filename_path, filename_path_out, 16)
    elif enc_mode == "dencrypt":
        decrypt_file(key, filename_path, filename_path_out, 16)