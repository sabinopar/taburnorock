from flask import render_template, request, flash, redirect, url_for, Response, session, abort, send_file
from werkzeug.utils import secure_filename
from flask_login import LoginManager, login_required, login_user, logout_user
from models import User
from settings import app, UPLOAD_FOLDER, os, ALLOWED_EXTENSIONS
import subprocess
from zxcvbn_crypt import zxcvbn_crypt
from chatbot_app import chatbot
from services import view_file_service
import dbmanager

login_manager = LoginManager()
login_manager.init_app(app)


@app.route("/")
def index():
    return render_template('index.html')


# list files view
@app.route('/view/', defaults={'path': ''}, methods=['GET'])
@app.route('/view/<path:path>', methods=['GET'])
@login_required
def view_files(path):
    relative_path, out =  view_file_service(path)
    file_list = out[0].splitlines()
    return render_template('view.html', fileslist=file_list, path=relative_path)


@app.route('/return-files/<path:filename>', methods=['GET', 'POST'])
def return_files_tut(filename):
    try:

        return send_file(os.path.join(UPLOAD_FOLDER, filename), attachment_filename=filename)
    except Exception as e:
        return str(e)


# file encryption
@app.route('/encrypt-files/<path:filename>', methods=['GET', 'POST'])
def encrypt_files_tut(filename):
    pass_from_input = request.form.get('password_enc')

    if  pass_from_input:
        password = request.form['password_enc']
        filename_path = os.path.join(UPLOAD_FOLDER, filename)
        zxcvbn_crypt("encrypt", filename_path, None, password.encode('utf-8'))

        return redirect(url_for('view_files', path=os.path.dirname('')))
    else:
        try:
            return Response('''
           <form action="" method="post">
             <p><input type=password name=password_enc>
             <p><input type=submit value=Encry>
          </form>
           ''')
        except Exception as e:
            return str(e)


# file descryption
@app.route('/dencrypt-files/<path:filename>', methods=['GET', 'POST'])
def dencrypt_files_tut(filename):
    pass_from_input = request.form.get('password_denc')

    if  pass_from_input:
        password = request.form['password_denc']
        filename_path = os.path.join(UPLOAD_FOLDER, filename)
        filename_out = os.path.join(UPLOAD_FOLDER, filename[:-1])
        zxcvbn_crypt("dencrypt", filename_path, None, password.encode('utf-8'))

        return redirect(url_for('view_files', path=os.path.dirname('')))
    else:
        try:
            return Response('''
           <form action="" method="post">
             <p><input type=password name=password_denc>
             <p><input type=submit value=Dencry>
          </form>
            ''')
        except Exception as e:
            return str(e)


@app.route('/handler', methods=['GET', 'POST'])
def hanlder():
    if request.method == 'POST':
        file_list_to_use = request.form.getlist("files_to_use")
        print(file_list_to_use)
        if request.form.get('toolbut') and len(file_list_to_use) > 0:
            if request.form['toolbut'] == 'delete':
                return delete_files(request)
            elif request.form['toolbut'] == 'decrypt':
                return redirect(url_for('dencrypt_files_tut', filename=file_list_to_use[0]))
            elif request.form['toolbut'] == 'encrypt':
                return redirect(url_for('encrypt_files_tut', filename=file_list_to_use[0]))
            elif request.form['toolbut'] == 'download':
                return redirect(url_for('return_files_tut', filename=file_list_to_use[0]))
        else:
            flash('Select a file')
    return redirect(url_for('view_files', path=os.path.dirname('')))


# delete files
#@app.route('/delete-files/', methods=['GET', 'POST'])
def delete_files(request):
    file_list_to_use = request.form.getlist("files_to_use")

    filename_paths = list(map(lambda filename: UPLOAD_FOLDER + "/" + filename, file_list_to_use))
    print(filename_paths)

    # if file exists, delete it
    try:
        [os.remove(filename) for filename in filename_paths]
    # if failed, report it back to the user
    except OSError as e:
        return str(e)
    return redirect(url_for('view_files', path=os.path.dirname('')))


# LOAD A FILE
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/load', methods=['GET', 'POST'])
@login_required
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit a empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return redirect(url_for('upload_file',
                                    filename=filename))
    return render_template('loadfile.html')


# somewhere to login
@app.route("/login", methods=["GET", "POST"])
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']

        users = dbmanager.dbmanager(username)

        if len(users) >= 1 and password == users[0][2]:
            id_user = users[0][0]
            user = User(str(id_user)+users[0][1])
            login_user(user)
            session['logged_in'] = True
            return redirect(url_for("index"))
        else:
            return abort(401)
    else:
        return render_template('login.html')


# somewhere to logout
@app.route("/logout")
@login_required
def logout():
    logout_user()
    session.pop('logged_in', None)
    return redirect(url_for("index"))


# handle login failed
@app.errorhandler(401)
def page_not_found(e):
    return Response('<p>Login failed</p>')


# callback to reload the user object
@login_manager.user_loader
def load_user(user_id):
    return User(user_id)


# bot page
@app.route("/chatapp")
@login_required
def home():
    return render_template("chatapp.html")


@app.route("/get")
def get_bot_response():
    user_text = request.args.get('msg')
    return str(chatbot.get_response(user_text))

########################


if __name__ == "__main__":
    app.run(host = '0.0.0.0', port=5000, debug=True)
