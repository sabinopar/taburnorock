import os
from flask import Flask

cwd = os.getcwd() + "/UPLOAD_FILES"
UPLOAD_FOLDER = cwd

ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'py', 'zip'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
# config
app.config.update(
    DEBUG=True,
    SECRET_KEY='secret_xxx'
)
