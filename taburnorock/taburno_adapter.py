from services import view_file_service
from chatterbot.logic import LogicAdapter

class TaburnoAdapter(LogicAdapter):
    def __init__(self, chatbot, **kwargs):
        super().__init__(chatbot, **kwargs)
        self.input_text = kwargs.get('input_text')

    def can_process(self, statement):

        if statement.text == self.input_text:
            print("can_process")
            print(statement)
            return True

        return False

    def process(self, input_statement, additional_response_selection_parameters=None):
        from chatterbot.conversation import Statement
        user_input = input_statement.text.lower()
        self.response_statement = Statement(text=input_statement)
        print("process")
        print(user_input)
        if user_input == self.input_text:

            relative_path, out = view_file_service("")
            output_text = out[0].splitlines()
            out = ""
            for line in output_text:
                out = out + "\n" + line.decode("utf-8")
            self.response_statement.text = out
            self.response_statement.confidence = 1
            return self.response_statement

        else:
            self.response_statement.confidence = 0

        return self.response_statement
