from distutils.core import setup

setup(
    name='taburnorock',
    version='1.0.0',
    packages=['taburnorock'],
    install_requires=['Flask', 'Flask_Login', 'pycryptodome', 'zxcvbn', 'Chatterbot'],
    url='https://gitlab.com/sabinopar/taburnorock/',
    download_url='https://gitlab.com/sabinopar/taburnorock/-/archive/master/taburnorock-master.tar.gz',
    license='GPL3',
    author='Sabino Parziale',
    author_email='sprab@onenetbeyond.org',
    description='Taburnorock is a small personalcloud app written in Python with Flask web framework.',
    keywords=['taburnorock', 'cloud', 'security'],
)